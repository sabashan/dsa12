package bcas.dsa.array;

public class RandomArrayDemo {

	public static void main(String[] args) {

		final String msg1 = "Please Enter the length of the array : ";
		final String msg2 = "Please Enter the random boundary : ";
		
		final int startBound =97;
		final int endBound =122;

		RandomArray ra = new RandomArray();

		//int array[] = ra.generateRandomWithRange(ra.readNumber(msg1), ra.readNumber(msg2));
		
		int textArray[]=ra.generateRandomWithRange(ra.readNumber(msg1), startBound,endBound);
		
		//ra.displayArray(textArray);
		
		ra.displayTextArray(textArray);

	}
}
