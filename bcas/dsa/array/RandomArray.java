package bcas.dsa.array;

import bcas.dsa.util.DsaUtil;

public class RandomArray extends DsaUtil {

	public void displayArray(int num[]) {
		for (int i : num) {
			System.out.print(i + " ");
		}
		
		System.out.println();
	}
	
	public void displayTextArray(int num[]) {
		for (int i : num) {
			System.out.print((char)i + " ");
		}
		System.out.println();
	}
}
